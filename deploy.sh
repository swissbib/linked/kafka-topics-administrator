#!/usr/bin/env bash

TARGET_DIR=/swissbib_index/apps/kafka-topics-administrator

./gradlew clean assembleDist

mkdir -p ${TARGET_DIR} &&  rm -rf ${TARGET_DIR}/* && tar xf ./build/distributions/kafka-topics-administrator-*.tar.gz -C ${TARGET_DIR} --strip-components=1

