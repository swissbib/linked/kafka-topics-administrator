/*
 * kafka-topics-administrator - client for managing Kafka topics
 * Copyright (C) 2019  project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package org.swissbib.linked;

import com.beust.jcommander.JCommander;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) throws InterruptedException {
        CommandMain commandMain = new CommandMain();
        CommandCreate commandCreate = new CommandCreate();
        CommandDelete commandDelete = new CommandDelete();
        CommandList commandList = new CommandList();
        CommandConsume commandConsume = new CommandConsume();
        CommandConsumerGroups commandConsumerGroups = new CommandConsumerGroups();

        JCommander jc = JCommander.newBuilder()
                .addObject(commandMain)
                .addCommand("create", commandCreate)
                .addCommand("delete", commandDelete)
                .addCommand("list", commandList)
                .addCommand("consume", commandConsume)
                .addCommand("groups", commandConsumerGroups)
                .build();
        jc.parse(args);

        if (jc.getParsedCommand() == null || commandMain.isHelp()) {
            jc.usage();
            System.exit(0);
        }
        if (commandCreate.isHelp()) {
            jc.usage("create");
            System.exit(0);
        }
        if (commandDelete.isHelp()) {
            jc.usage("delete");
            System.exit(0);
        }
        if (commandList.isHelp()) {
            jc.usage("list");
            System.exit(0);
        }
        if (commandConsume.isHelp()) {
            jc.usage("consume");
            System.exit(0);
        }

        if (commandConsumerGroups.isHelp()) {
            jc.usage("groups");
            System.exit(0);
        }
        if (commandMain.debug) {
            Configurator.setRootLevel(Level.DEBUG);
        }

        Properties appProperties = new Properties();
        try {
            appProperties.load(new FileInputStream(commandMain.getAppConfigPath()));
        } catch (IOException e) {
            System.out.println("Error: Can't load configuration file (path: " + commandMain.getAppConfigPath() + ")!");
            System.exit(1);
        }

        File topicsConfigPath = null;
        if (commandMain.getTopicsConfigPath() != null) {
            topicsConfigPath = commandMain.getTopicsConfigPath();
        } else if (appProperties.getProperty("topics.config.path") != null) {
            topicsConfigPath = new File(appProperties.getProperty("topics.config.path"));
        } else {
            try {
                topicsConfigPath = new File(Objects.requireNonNull(App.class.getClassLoader().getResource("topics.yaml")).toURI());
            } catch (URISyntaxException e) {
                System.out.println("Error in URI syntax");
            }
        }
        try {
            if (topicsConfigPath == null) {
                throw new IOException("Error: Can't load topic configuration file (no path indicated)!");
            } else if (!topicsConfigPath.exists()) {
                throw new IOException("Error: Can't load topic configuration file (path: " + topicsConfigPath.toString() + ")!");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

        YamlParser yamlParser;
        yamlParser = new YamlParser(topicsConfigPath);
        appProperties.remove("topics.config.path");
        KafkaAdminClient adminClient = new KafkaAdminClient(appProperties);

        switch (jc.getParsedCommand()) {
            case "create":
                if (!commandCreate.isAllTopics() && commandCreate.getTopics() == null) {
                    System.out.println("No topics defined: Either a list of topics or the --all flag is required");
                    System.exit(1);
                }
                List<Topic> createTopics = commandCreate.isAllTopics() ?
                        yamlParser.getFullTopics() :
                        yamlParser.getFullTopics().stream()
                                .filter((t) -> commandCreate.getTopics().contains(t.getName())).collect(Collectors.toList());
                adminClient.create(createTopics, commandCreate);
                break;
            case "delete":
                if (!commandDelete.isAllTopics() && commandDelete.getTopics() == null) {
                    System.out.println("No topics defined: Either a list of topics or the --all flag is required");
                    System.exit(1);
                }
                List<String> deleteTopics = commandDelete.isAllTopics() ?
                        yamlParser.getTopics() :
                        commandDelete.getTopics();
                adminClient.delete(deleteTopics);
                break;
            case "list":
                if (!commandList.isAllTopics() && commandList.getTopics() == null && !commandList.searchTopics) {
                    System.out.println("No topics defined: Either a list of topics or the --all flag is required");
                    System.exit(1);
                }
                List<String> listTopics;
                if (commandList.isAllTopics()) {
                    listTopics = yamlParser.getTopics();
                } else if (commandList.searchTopics) {
                    listTopics = adminClient.searchTopicNames();
                } else {
                    listTopics = commandList.getTopics();
                }
                switch (commandList.getDisplayedInformation()) {
                    case "short":
                        adminClient.showMinimalList(listTopics);
                        break;
                    case "extended":
                        adminClient.showExtendedList(listTopics);
                        break;
                    case "configs":
                        adminClient.showConfig(listTopics);
                        break;
                    case "size":
                        adminClient.showSize(listTopics, appProperties.getProperty("bootstrap.servers"));
                        break;
                    default:
                        System.out.println(commandList.getDisplayedInformation() + " is no valid display option. Choose from 'short', 'extended', 'configs', and 'offsets'.");
                        System.exit(1);
                        break;
                }
                break;
            case "consume":
                if (commandConsume.hasTopic())
                {
                    try {
                        commandConsume.init(appProperties.getProperty("bootstrap.servers"), adminClient);

                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                        System.exit(1);
                    }
                    commandConsume.consume();
                } else {
                    System.out.println("No valid topic supplied to consume from.");
                    System.exit(1);
                }
                break;
            case "groups":
                if (commandConsumerGroups.all()) {
                    adminClient.showAllConsumerGroups();

                } else if (commandConsumerGroups.delete()) {
                    if (commandConsumerGroups.hasGroups()) {
                        adminClient.deleteConsumerGroups(commandConsumerGroups.getGroups());
                    } else {
                        System.out.println("Error: No consumer groups supplied to delete!");
                        System.exit(1);
                    }
                } else if (commandConsumerGroups.hasGroups()) {
                    adminClient.showConsumerGroups(commandConsumerGroups.getGroups(), appProperties.getProperty("bootstrap.servers"));
                }  else{
                    System.out.println("Either display all groups with -a or specify a specific group (or list of groups) to be displayed or deleted (-d)!");
                    System.exit(1);
                }
        }
        adminClient.close();
    }

}
