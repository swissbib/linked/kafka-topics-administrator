package org.swissbib.linked;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.*;

@Parameters(commandDescription = "Consume messages from a topic and output them to stdout.")
class CommandConsume {

    @Parameter(description = "The topics to consume messages from.")
    private List<String> topics;

    @Parameter(names = "-i", description = "The number of messages to consume.")
    private int count = 100;

    @Parameter(names = "-p", description = "Partition to consume from (default no partition).")
    private int partition = -1;

    @Parameter(names = "-o", description = "Offset to consume from, requires a parition to be set.")
    private int offset = 0;

    @Parameter(names = {"-h", "--help"}, description = "Show this help message", help = true)
    private boolean help;

    boolean hasTopic() {
        return topics != null && !topics.isEmpty() && topics.get(0) != null;
    }

    boolean isHelp() {
        return help;
    }

    private KafkaConsumer<String, String> consumer;

    void init(String brokers, KafkaAdminClient client) throws Exception {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", brokers);
        properties.put("group.id", "admin-consumer-" + new Random().nextInt());
        properties.put("enable.auto.commit", "true");
        properties.put("auto.offset.reset", "earliest");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<>(properties);
        List<String> existingTopics = client.searchTopicNames();
        if (existingTopics.contains(topics.get(0))) {
            consumer.subscribe(Collections.singleton(topics.get(0)));
        } else {
            throw new Exception("No topic with name " + topics.get(0) + " found.");
        }
    }

    void consume() {
        int countRecords = count;
        System.out.println("Searching Messages (This may take a few seconds!)");
        if (partition != -1) {
            consumer.unsubscribe();
            consumer.assign(Collections.singleton(new TopicPartition(topics.get(0), partition)));
            if (offset != 0) {
                consumer.seek(new TopicPartition(topics.get(0), partition), offset);
            }
        }
        System.out.println("Read in topic "+ topics.get(0) + " in parition " + partition + "from offset " + consumer.position(new TopicPartition(topics.get(0), partition)));
        while (countRecords > 0) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(200));
            for (ConsumerRecord<String, String> record : records) {
                System.out.println("Key:" + record.key());
                System.out.println("Message:\n" + record.value());
                System.out.println("#################################################################################");
                countRecords -= 1;
            }
        }

    }

}
