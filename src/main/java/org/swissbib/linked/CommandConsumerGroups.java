package org.swissbib.linked;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.List;

@Parameters(commandDescription = "Read descriptions of consumer groups.")
class CommandConsumerGroups {
    @Parameter(description = "Comma-separated list of consumer groups to display.")
    private List<String> groups;

    @Parameter(names = {"-a", "--all"}, description = "Display all consumer groups.")
    private boolean allConsumerGroups = false;

    @Parameter(names = {"-d", "--delete"}, description = "Delete the consumer groups.")
    private boolean deleteConsumerGroups = false;

    @Parameter(names = {"-h", "--help"}, description = "Show this help message", help = true)
    private boolean help;

    boolean isHelp() { return help; }

    boolean all() {return  allConsumerGroups;}

    boolean delete() {return deleteConsumerGroups;}

    boolean hasGroups() {
        return groups != null && !groups.isEmpty();
    }

    List<String> getGroups() {
        return groups;
    }
}
