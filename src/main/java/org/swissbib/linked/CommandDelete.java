/*
 * kafka-topics-administrator - client for managing Kafka topics
 * Copyright (C) 2019  project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.List;

@Parameters(commandDescription = "Deletes one or more topics")
public class CommandDelete {
    @Parameter(description = "Comma-separated list of topics to delete")
    private List<String> topics;

    @Parameter(names = {"-a", "--all"}, description = "Delete all topics")
    private boolean allTopics = false;

    @Parameter(names = {"-h", "--help"}, description = "Show this help message", help = true)
    private boolean help;

    public List<String> getTopics() {
        return topics;
    }

    boolean isAllTopics() {
        return allTopics;
    }

    boolean isHelp() {
        return help;
    }
}
