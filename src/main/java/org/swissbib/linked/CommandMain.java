/*
 * kafka-topics-administrator - client for managing Kafka topics
 * Copyright (C) 2019  project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked;

import com.beust.jcommander.*;

import java.io.File;

@SuppressWarnings("ConstantConditions")
class CommandMain {
    @Parameter(names = {"-t", "--topics"}, description = "Path to topics configuration file", converter = FileConverter.class)
    private File topicsConfigPath;

    @Parameter(names = {"-c", "--config"}, description = "Path to application configuration file.", converter = FileConverter.class)
    private File appConfigPath =
            new File(getClass().getClassLoader().getResource("config.properties").getFile()).exists() ?
                    new File(getClass().getClassLoader().getResource("config.properties").getFile()) :
                    new File("./config/config.properties");
    @Parameter(names = {"-d", "--debug"}, description = "Run in debug mode. Logging set to debug level.")
    public boolean debug = false;

    @Parameter(names = {"-h", "--help"}, description = "Show this help message", help = true)
    private boolean help = false;


    File getTopicsConfigPath() {
        return topicsConfigPath;
    }

    File getAppConfigPath() {
        return appConfigPath;
    }

    public boolean isHelp() {
        return help;
    }
}

class FileConverter implements IStringConverter<File> {
    @Override
    public File convert(String value) {
        return new File(value);
    }
}
