/*
 * kafka-topics-administrator - client for managing Kafka topics
 * Copyright (C) 2019  project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked;

import org.apache.kafka.clients.admin.*;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.TopicPartitionInfo;
import org.apache.kafka.common.config.ConfigResource;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

class KafkaAdminClient {
    private AdminClient adminClient;

    KafkaAdminClient(Properties props) {
        adminClient = AdminClient.create(props);
    }

    private KafkaConsumer consumer(String bootstrapServers, String offset) {
        Properties props = new Properties();
        props.setProperty("bootstrap.servers", bootstrapServers);
        props.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.setProperty("auto.offset.reset", offset);
        props.setProperty("enable.auto.commit", "false");
        return new KafkaConsumer(props);
    }

    List<String> searchTopicNames() {
        try {
            Set<String> topics = adminClient.listTopics().names().get();
            return new ArrayList<>(topics);
        } catch (ExecutionException | InterruptedException ex) {
            System.out.println(ex.getMessage());
            System.exit(1);
            return new ArrayList<>();
        }
    }

    void create(List<Topic> topics, CommandCreate configs) {
        adminClient.createTopics(topicsToNewTopicList(topics, configs.getSettings()))
                .values().forEach((topicName, v) -> v.whenComplete((voidValue, ex) -> {
            if (ex != null) System.out.println("Error for topic " + topicName + ": " + ex.getMessage());
            else System.out.println("Creation of topic " + topicName + " succeeded");
        }));
    }

    void delete(List<String> topics) {
        adminClient.deleteTopics(topics)
                .values().forEach((topicName, v) -> v.whenComplete((voidValue, ex) -> {
            if (ex != null) System.out.println("Error for topic " + topicName + ": " + ex.getMessage());
            else System.out.println("Deletion of topic " + topicName + " succeeded");
        }));
    }

    void showSize(List<String> topics, String bootstrapServers) {
        Collections.sort(topics);
        KafkaConsumer earliestConsumer = consumer(bootstrapServers, "earliest");
        KafkaConsumer latestConsumer = consumer(bootstrapServers, "latest");
        adminClient.describeTopics(topics)
                .values().forEach((topicName, v) -> v.whenComplete((td, ex) -> {
            if (ex != null)
                System.out.println("Error for topic " + topicName + ": " + ex.getMessage());
            else {
                List<TopicPartition> topicPartitions =
                        td.partitions().stream().map((p) -> new TopicPartition(topicName, p.partition())).collect(Collectors.toList());
                earliestConsumer.assign(topicPartitions);
                latestConsumer.assign(topicPartitions);
                long total = 0;
                List<String> sizes = new ArrayList<>();
                for (TopicPartition tp : topicPartitions) {
                    long partitionSize = latestConsumer.position(tp) - earliestConsumer.position(tp);
                    total += partitionSize;
                    sizes.add("  - Partition #" + tp.partition() + ": " + partitionSize);
                }
                System.out.println("# of messages in topic " + topicName + ": " + total);
                for (String s : sizes) {
                    System.out.println(s);
                }

            }
        }));
    }

    void showExtendedList(List<String> topics) {
        Collections.sort(topics);
        adminClient.describeTopics(topics)
                .values().forEach((topicName, v) -> v.whenComplete((td, ex) -> {
            if (ex != null)
                System.out.println("Error for topic " + topicName + ": " + ex.getMessage());
            else {
                System.out.println("\nPARTITIONS FOR TOPIC " + topicName.toUpperCase());
                for (TopicPartitionInfo tpi : td.partitions()) {
                    System.out.println("- Partition #" + tpi.partition());
                    System.out.print("  - Leader partition on broker #" + tpi.leader().id() + " (" + tpi.leader().host() + ":" + tpi.leader().port() + ")");
                    System.out.println(tpi.leader().hasRack() ? " belonging to rack " + tpi.leader().rack() : "");
                    for (Node replica : tpi.replicas()) {
                        System.out.print("  - Replica partition on broker #" + replica.id() + " (" + replica.host() + ":" + replica.port() + ")");
                        System.out.println(replica.hasRack() ? " (belongs to rack " + replica.rack() + ")" : "");
                    }
                }
                System.out.println("------------------------------------------------");
            }
        }));
    }

    void showConfig(List<String> topics) {
        List<ConfigResource> resources = new ArrayList<>();
        Collections.sort(topics);
        for (String t : topics) {
            ConfigResource resource = new ConfigResource(ConfigResource.Type.TOPIC, t);
            resources.add(resource);
        }
        adminClient.describeConfigs(resources).values().forEach((cr, v) -> v.whenComplete((desc, ex) -> {
            if (ex != null)
                System.out.println("Error for topic " + cr.name() + ": " + ex.getMessage());
            else {
                System.out.println("\nCONFIGURATION FOR TOPIC " + cr.name().toUpperCase());
                for (ConfigEntry ce : desc.entries()) {
                    System.out.println(ce.name() + ": " + ce.value());
                }
                System.out.println("------------------------------------------------");
            }
        }));
    }

    void showMinimalList(List<String> topics) {
        int width = topics.stream().reduce("", (x, a) -> x.length() > a.length() ? x : a).length();
        adminClient.listTopics()
                .names().whenComplete((activeTopics, ex) -> {
                    Collections.sort(topics);
                    for (String topic : topics) {
                        System.out.println(String.format("%" + width + "s  [%s]", topic, activeTopics.contains(topic) ? "OK" : "NA"));
                    }
        });
    }

    void showAllConsumerGroups() throws InterruptedException {
        ListConsumerGroupsResult groups = adminClient.listConsumerGroups();
        KafkaFuture<Collection<ConsumerGroupListing>> listingsFuture = groups.valid();
        while (!listingsFuture.isDone()) {
            System.out.println("Waiting for request to return.");
            TimeUnit.SECONDS.sleep(1);
        }
        System.out.println("Consumer Group IDs:");
        listingsFuture.whenComplete((consumerGroupListings, throwable) -> {
                ArrayList<String> groupIds = new ArrayList<>();
                for (ConsumerGroupListing consumerGroupListing : consumerGroupListings) {
                    groupIds.add(consumerGroupListing.groupId());
                }
                Collections.sort(groupIds);
                groupIds.forEach(s -> System.out.println("    " + s));
        });
    }

    void deleteConsumerGroups(List<String> groups) {
        adminClient.deleteConsumerGroups(groups);
        System.out.println("Deleted consumer groups: ");
        groups.forEach(s -> System.out.println(String.format("    %s", s)));
    }

    void showConsumerGroups(List<String> groups, String bootstrapServers) {
        KafkaConsumer latestConsumer = consumer(bootstrapServers, "latest");
        for (String group : groups) {
            try {
                Map<TopicPartition, OffsetAndMetadata> result = adminClient.listConsumerGroupOffsets(group).partitionsToOffsetAndMetadata().get();
                latestConsumer.assign(result.keySet());
                System.out.println("Topic                         | Partition | Current Offset | Latest Offset  | Lag");
                ArrayList<String> outputs = new ArrayList<String>();
                long totalCurrent = 0;
                long totalLatest = 0;
                long totalLag = 0;
                for (TopicPartition partition : result.keySet()) {
                    long latestPosition = latestConsumer.position(partition);
                    totalLatest += latestPosition;
                    OffsetAndMetadata offsetAndMetadata = result.get(partition);
                    long currentPosition = offsetAndMetadata.offset();
                    totalCurrent += currentPosition;
                    long lag = latestPosition - currentPosition;
                    totalLag += lag;
                    outputs.add(String.format("%-30s| %-9d | %,14d | %,14d | %,14d",
                            partition.topic(),
                            partition.partition(),
                            currentPosition,
                            latestPosition,
                            lag));
                }
                outputs.add(String.format("%-30s|           | %,14d | %,14d | %,14d",
                        "xTotal",
                        totalCurrent,
                        totalLatest,
                        totalLag));
                Collections.sort(outputs);
                for (String s : outputs) {
                    System.out.println(s);
                }
            } catch (ExecutionException | InterruptedException ex) {
                System.out.println(ex.getMessage());
                System.exit(1);
            }
        }
        latestConsumer.close();
    }

    void close() {
        adminClient.close();
    }

    private static List<NewTopic> topicsToNewTopicList(List<Topic> topics, List<String> overrides) {
        if (overrides != null) {
            Map<String, String> overridesMap = overrides.stream()
                    .collect(Collectors.toMap((c) -> c.split("=")[0], (c) -> c.split("=")[1]));
            List<NewTopic> newTopics = new ArrayList<>();
            for (Topic t : topics) {
                int partitions = overridesMap.containsKey("partitions") ? Integer.parseInt(overridesMap.get("partitions")) : t.getPartitions();
                short replicationFactor = overridesMap.containsKey("replicationFactor") ? Short.parseShort(overridesMap.get("replicationFactor")) : t.getReplicationFactor();
                NewTopic nt = new NewTopic(t.getName(), partitions, replicationFactor);
                if (t.getConfigurations() != null) {
                    nt.configs(t.getConfigurations().stream()
                            .map((c) -> c.split("="))
                            .map((c) -> {
                                List<String> newPair = new ArrayList<>();
                                newPair.add(c[0]);
                                newPair.add(overridesMap.containsKey(c[0]) ? overridesMap.get(c[0]) : c[1]);
                                return newPair;
                            })
                            .collect(Collectors.toMap((c) -> c.get(0), (c) -> c.get(1))));
                }
                newTopics.add(nt);
            }
            return newTopics;
        } else {
            List<NewTopic> newTopics = new ArrayList<>();
            for (Topic t : topics) {
                NewTopic nt = new NewTopic(t.getName(), t.getPartitions(), t.getReplicationFactor());
                if (t.getConfigurations() != null) {
                    nt.configs(t.getConfigurations().stream()
                            .collect(Collectors.toMap((c) -> c.split("=")[0], (c) -> c.split("=")[1])));
                }
                newTopics.add(nt);
            }
            return newTopics;
        }
    }
}
