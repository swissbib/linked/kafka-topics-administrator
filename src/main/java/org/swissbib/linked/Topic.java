/*
 * kafka-topics-administrator - client for managing Kafka topics
 * Copyright (C) 2019  project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Topic {
    private String name;
    private int partitions;
    private short replicationFactor;
    private List<String> configurations;

    public Topic() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPartitions() {
        return partitions;
    }

    public void setPartitions(int partitions) {
        this.partitions = partitions;
    }

    public short getReplicationFactor() {
        return replicationFactor;
    }

    public void setReplicationFactor(short replicationFactor) {
        this.replicationFactor = replicationFactor;
    }

    public List<String> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(List<String> configurations) {
        this.configurations = configurations;
    }

    public Topic addDefaultValues(Topic defaultTopic) {
        Topic mergedTopic = new Topic();
        mergedTopic.setName(this.getName());
        mergedTopic.setPartitions(this.getPartitions() == 0 ? defaultTopic.getPartitions() : this.getPartitions());
        mergedTopic.setReplicationFactor(this.getReplicationFactor() == 0 ?
                defaultTopic.getReplicationFactor() : this.getReplicationFactor());
        List<String> newConfigs = new ArrayList<>();
        if (this.getConfigurations() != null) {
            Set<String> configKeys = this.getConfigurations().stream()
                    .map((v) -> v.split("=")[0])
                    .collect(Collectors.toSet());
            newConfigs = defaultTopic.getConfigurations() != null ?
                    Stream.concat(defaultTopic.getConfigurations().stream()
                                    .filter((v) -> !configKeys.contains(v.split("=")[0])),
                            this.getConfigurations().stream()).collect(Collectors.toList()) :
                    this.getConfigurations();
        } else if (defaultTopic.getConfigurations() != null) {
            newConfigs = defaultTopic.getConfigurations();
        } else {
            newConfigs = null;
        }
        mergedTopic.setConfigurations(newConfigs);
        return mergedTopic;
    }
}

