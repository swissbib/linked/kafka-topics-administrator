/*
 * kafka-topics-administrator - client for managing Kafka topics
 * Copyright (C) 2019  project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked;

import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class YamlParser {

    private Topics data;

    YamlParser(File path) {
        Constructor constructor = new Constructor(Topics.class);
        TypeDescription topicsDescription = new TypeDescription(Topics.class);
        topicsDescription.addPropertyParameters("defaults", Topic.class);
        topicsDescription.addPropertyParameters("topics", Topic.class);
        constructor.addTypeDescription(topicsDescription);
        Yaml yaml = new Yaml(constructor);
        try {
            data = yaml.load(new FileInputStream(path));
            checkName();
            //checkPartitions();
            //checkReplicationFactor();
            checkConfigurationSyntax();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<String> getTopics() {
        List<String> res = new ArrayList<>();
        for (Topic t : data.getTopics()) {
            res.add(t.getName());
        }
        return res;
    }

    public List<Topic> getFullTopics() {
        if (data.getDefaults() != null) {
            return data.getTopics().stream().map((t) -> t.addDefaultValues(data.getDefaults())).collect(Collectors.toList());
        }
        return data.getTopics();
    }

    private void checkPartitions() {
        if (data.getDefaults() != null && data.getDefaults().getPartitions() < 1) {
            System.out.println("Default number of partitions must be greater than 0");
            System.exit(1);
        }
        data.getTopics()
                .forEach((t) -> {
                    if (t.getPartitions() < 1) {
                        System.out.println("Number of partitions in topic " + t.getName() + " must be greater than 0");
                        System.exit(1);
                    }
                });
    }

    private void checkReplicationFactor() {
        if (data.getDefaults() != null && data.getDefaults().getReplicationFactor() < 1) {
            System.out.println("Default replication factor must be at least 1");
            System.exit(1);
        }
        data.getTopics()
                .forEach((t) -> {
                    if (t.getReplicationFactor() < 1) {
                        System.out.println("Replication factor of topic " + t.getName() + " must be at least 1");
                        System.exit(1);
                    }
                });

    }

    private void checkName() {
        data.getTopics().forEach((t) -> {
            if (t.getName() == null) {
                System.out.println("Topic must have a name");
                System.exit(1);
            }
        });
    }

    private void checkConfigurationSyntax() {
        if (data.getDefaults() != null && data.getDefaults().getConfigurations() != null) {
            data.getDefaults().getConfigurations().forEach((t) -> {
                if (!t.contains("=") || t.split("=")[0].length() == 0 || t.split("=")[1].length() == 0) {
                    System.out.println("Default configuration tokens must be provided in the form of key=value");
                    System.exit(1);
                }
            });
        }
        data.getTopics().forEach((t) -> {
            if (t.getConfigurations() != null) {
                t.getConfigurations().forEach((c) -> {
                    if (!c.contains("=") || c.split("=")[0].length() == 0 || c.split("=")[1].length() == 0) {
                        System.out.println("Configuration tokens of topic " + t.getName() + " must be provided kin the form of key=value");
                        System.exit(1);
                    }
                });
            }
        });
    }
}
